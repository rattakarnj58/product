import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-address',
  templateUrl: './address.page.html',
  styleUrls: ['./address.page.scss'],
})
export class AddressPage implements OnInit {

  num: any;
  id:any;
  img:any;
  constructor(private navCtrl: NavController,
    public route:Router
  ){}
  ngOnInit() {
    this.id = history.state.id
    this.num = history.state.num
    console.log(this.id);
  }
  backButton(id){
    this.route.navigate(['choose'],{state:{id:id}});
  }
}
