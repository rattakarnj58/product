import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, IonSegment } from '@ionic/angular';
import { ChoosePage } from '../choose/choose.page';
import { Router , NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  storageValue; any;
  counter: any;
  quantities: any;
  id : any = "Pink Wallet"
  price : any = "฿120"
  slideOption = {
    pagination: {
      el: '.swiper-pagination',
      // clickable: true,
      dynamicBullets: true,
    }
  }
  constructor(private nav: NavController,
    private route : Router
    ) {
    
  }
  ngOnInit() {

    
  }
    

  choose(id,price){
    
    this.route.navigate(['choose'],{state:{data:id,price:price}});
    console.log(id,price);
    
  }



  slider = [{
    image: "assets/img/e.jpg",
  },
  {
    image: "assets/img/c.jpeg",
  },
  {
    image: "assets/img/d.png",
  }]
  
}


  


