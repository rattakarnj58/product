import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonSegment } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.page.html',
  styleUrls: ['./choose.page.scss'],
})
export class ChoosePage implements OnInit {
  qty: any;
  id:any;
  img:any;
  constructor(private navCtrl: NavController,
    public route:Router
    ) {
    this.qty = 1;

  }

  ngOnInit() {
    this.id = history.state.data
    console.log(">>>>",this.id);
    
  }
  backButton() {
    this.route.navigate(['home']);
  }


  increment() {
    console.log(this.qty + 1);
    this.qty += 1;
  }

  decrement() {
    if (this.qty - 1 < 1) {
      this.qty = 1
      console.log("1->" + this.qty);
    } else {
      this.qty -= 1;
      console.log("2->" + this.qty);
    }
  }

  address(id,qty){
    // this.route.navigate(['address'],{state:{id:id,num:qty}});
    console.log("id = "+id," num = "+qty);
    
  }

}
